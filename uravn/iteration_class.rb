# frozen_string_literal: true

require 'gnuplot'
require 'byebug'

class IterationClass
  def initialize(eps:)
    @eps = eps
  end

  def call(x1,x2)
    x = f_left(x1.to_f)
    result = {}
    count = 1
    result[count] = x
    count = count.next
    result[count] = f_left(result[count - 1])
    while @eps < (result[count] - result[count - 1]).abs
      count = count.next
      result[count] = f_left(result[count - 1])
    end

    result.each do |k, v|
      puts "Итерация для левого корня #{k}, значение х:#{v}\n"
    end
    puts

    x = f_right(x2.to_f)
    result = {}
    count = 1
    result[count] = x
    count = count.next
    result[count] = f_right(result[count - 1])
    while @eps < (result[count] - result[count - 1]).abs
      count = count.next
      result[count] = f_right(result[count - 1])
    end

    result.each do |k, v|
      puts "Итерация для правого корня #{k}, значение х:#{v}\n"
    end
  end

  private

  def f_left(x)
    # Сходимость производной к левому корю
    0.2*(x**4-2*x**2 -x -1)+x
  end

  def f_right(x)
    # Сходимость производной к правому
    Math.sqrt(Math.sqrt(2*x**2+x+1))
  end
end
