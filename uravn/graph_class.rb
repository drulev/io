# frozen_string_literal: true

require 'gnuplot'
require 'byebug'
class GraphClass
  def initialize(f1:, f2:)
    @f1 = f1
    @f2 = f2
  end

  def call
    o = Gnuplot.open do |gp|
      Gnuplot::Plot.new(gp) do |plot|
        green = 0
        red = 0
        plot.terminal 'svg size 1920,1080 font "Helvetica,30"'
        plot.xrange '[-2:2]'
        plot.yrange '[-4:4]'
        plot.title  'Пересечение функций, есть корни'
        plot.ylabel 'x'
        plot.xlabel 'y'
        plot.data << Gnuplot::DataSet.new(@f1) do |ds|
          ds.with = 'lines'
          ds.linewidth = 4
        end
        plot.data << Gnuplot::DataSet.new(@f2) do |ds|
          ds.with = 'lines'
          ds.linewidth = 4
        end
      end
    end
    File.write 'graph_method.svg', o
  end
end
