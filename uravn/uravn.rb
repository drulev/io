# frozen_string_literal: true

require 'byebug'
require './iteration_class'
require './graph_class'
puts 'Итерационный метод:\n'
IterationClass.new(eps: 0.0001).call(-1.5,2)
puts
puts 'Графический метод:\n'
GraphClass.new(f1: 'x+1', f2: 'x**4-2*x**2').call
puts 'See graph_methods.svg'
