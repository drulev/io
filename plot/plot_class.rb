# frozen_string_literal: true

require 'gnuplot'
require 'byebug'

class PlotClass
  MIN_X = -3
  MAX_X = 5
  MIN_Y = -5
  MAX_Y = 9
  S0 = (MAX_X - MIN_X).abs * (MAX_Y - MIN_Y).abs

  def initialize(x1 = -2.5, y1 = 2.5, a = 5)
    @x1 = x1
    @y1 = y1
    @x2 = x1 - a * Math.cos(60)
    @x3 = @x2
    @y2 = y1 + a
    @y3 = y1 - a
    @a = a 
    @real_s = Math.sqrt(3)*a*a/2
  end

  def call
    Dir.mkdir('rendered_files') unless File.directory?('rendered_files')
    seanse = []
    (1..10).to_a.each do |time|
      o = Gnuplot.open do |gp|
        Gnuplot::Plot.new(gp) do |plot|
          green = 0
          red = 0
          plot.terminal 'svg size 1920,1080 font "Helvetica,30"'
          plot.xrange '[-10:10]'
          plot.yrange '[-5:15]'
          plot.title  "Равносторонний треугольник со сторонами #{@a}"
          plot.ylabel 'x'
          plot.xlabel 'y'
          write_rectangle(plot)
          write_box(plot)
          time.times do
            100.times do
              rand_x = rand(MIN_X.to_f..MAX_X.to_f)
              rand_y = rand(MIN_Y.to_f..MAX_Y.to_f)
              plot.data << Gnuplot::DataSet.new([[rand_x], [rand_y]]) do |ds|
                ds.with = 'points lt -1 pt 5 pi 4 ps 0.8'
                ds.linewidth = 2
                ds.notitle
                ds.linecolor = if include_triangle?(rand_x, rand_y)
                  green = green.next
                  "rgb 'green'"
                else
                  red = red.next
                  "rgb 'red'"
                end
              end
            end
          end
          seanse << S0.to_f*green/red
          plot.label "'Среднее значение: #{seanse.inject(0, :+) / seanse.count} Площадь фигуры: #{seanse.last}, Реальная площадь: #{@real_s}' at -11,-7"
        end
      end

      File.write "rendered_files/#{time}.svg", o
    end
    puts 'See /rendered_files'
  end

  private

  def write_rectangle(plot)
    plot.data << Gnuplot::DataSet.new([[@x1, @x2], [@y1, @y2]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'green'"
      ds.notitle
    end
    plot.data << Gnuplot::DataSet.new([[@x2, @x3], [@y2, @y3]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'green'"
      ds.notitle
    end
    plot.data << Gnuplot::DataSet.new([[@x1, @x3], [@y1, @y3]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'green'"
      ds.notitle
    end
  end

  def write_box(plot)
    plot.data << Gnuplot::DataSet.new([[MIN_X, MIN_X], [MIN_Y, MAX_Y]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'red'"
      ds.notitle
    end
    plot.data << Gnuplot::DataSet.new([[MIN_X, MAX_X], [MIN_Y, MIN_Y]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'red'"
      ds.notitle
    end
    plot.data << Gnuplot::DataSet.new([[MAX_X, MAX_X], [MIN_Y, MAX_Y]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'red'"
      ds.notitle
    end
    plot.data << Gnuplot::DataSet.new([[MIN_X, MAX_X], [MAX_Y, MAX_Y]]) do |ds|
      ds.with = 'linespoints'
      ds.linewidth = 10
      ds.linecolor = "rgb 'red'"
      ds.notitle
    end
  end

  def get_k_b(x1, y1, x2, y2)
    # [k, b]
    k = (x2 - x1).zero? ? 0 : (y2 - y1) / (x2 - x1)
    b_ = k.zero? ? x2 : (y2 - k * x2)
    [k, b_]
  end

  def get_value(k, b, x, y)
    if k.zero?
      return 1 if x > b
      return -1 if x < b
    end
    value = k * x + b
    return 1 if value > y
    return -1 if value < y

    0
  end

  def include_triangle?(x, y)
    k1, b1 = get_k_b(@x1, @y1, @x2, @y2)
    k2, b2 = get_k_b(@x2, @y2, @x3, @y3)
    k3, b3 = get_k_b(@x1, @y1, @x3, @y3)

    return false if get_value(k1, b1, x, y) == -1
    return false if get_value(k2, b2, x, y) == 1
    return false if get_value(k3, b3, x, y) == 1

    true
  end
end
